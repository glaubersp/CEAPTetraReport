<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Relatório Tetra Analytix</title>
    <link rel="shortcut icon" href="images/favicon.png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="css/tetra.css">
    <link rel="stylesheet" href="css/ceap.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-12 report-view">
                <div class="report-header text-center">
                    <h1><?php include "./html/school_name.html" ?></h1>
                    <p class="school-level"><?php include "./html/grade_name.html" ?></p>
                    <h3>Apoio da Comunidade</h3>
                    <h4>Resposta dos Pais / Responsáveis</h4>
                </div>
                <div class="report-content" style="margin-top: 10px;">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-7 col-lg-8">
                            <div class="col-xs-3 col-md-3 col-sm-3 col-lg-2"><img src="images/clear-expectations-01.png" class="rel-img" alt="logo"></div>
                            <div class="col-xs-4 col-md-7 col-sm-4 col-lg-4">
                                <h2 style="margin: 1px 0 0 0;">Expectativas</h2>
                            </div>
                            <div class="col-xs-6 col-sm-9 col-lg-offset-1 col-lg-4"><img src="./images/P_CE_CM_slide.png" alt="Expectativas" style="margin-top: 10px;">
                                <!--<canvas id="P_CE_CM_slide" style="margin-top: 10px;" width="200" height="15"></canvas>-->
                            </div>
                            <div class="col-xs-12 col-lg-9" style="margin-top: 0px;">
                                <h4>Eu concordo com as prioridades estabelecidas pela escola</h4>
                            </div>
                            <div class="col-xs-12">
                                <p>O entendimento comum leva a expectativas claras. Uma maneira de se atingir o entendimento comum é buscar consenso quanto às prioridades e regras da escola, através de oportunidades para que essa construção seja feita
                                    de maneira aberta e colaborativa.</p>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
                            <?php include "./html/P_CE_CM_chart.html" ?>
                        </div>
                    </div>
                </div>
                <div class="report-content" style="margin-top: 10px;">
                    <div class="row row-gray" style="padding: 10px 0;">
                        <div class="col-xs-6 col-sm-6 col-md-7 col-lg-8">
                            <div class="col-xs-3 col-md-3 col-sm-3 col-lg-2"><img src="images/skill-building-01.png" class="rel-img" alt="logo"></div>
                            <div class="col-xs-4 col-md-7 col-sm-4 col-lg-4">
                                <h2 style="margin: 1px 0 0 0;">Habilidades</h2>
                            </div>
                            <div class="col-xs-6 col-sm-9 col-lg-offset-1 col-lg-4"><img src="./images/P_SB_CM_slide.png" alt="Habilidades" style="margin-top: 10px;">
                                <!--<canvas id="P_SB_CM_slide" style="margin-top: 10px;" width="200" height="15"></canvas>-->
                            </div>
                            <div class="col-xs-12 col-lg-9" style="margin-top: 0px;">
                                <h4>A direção da escola envolve a comunidade em suas atividades de maneira significativa</h4>
                            </div>
                            <div class="col-xs-12">
                                <p>O apoio da comunidade é indicado pela participação. Pais, responsáveis e familiares contribuem para o sucesso da escola quando se envolvem com outros membros da comunidade no planejamento, promoção, coordenação e apoio
                                    às atividades escolares.</p>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
                            <?php include "./html/P_SB_CM_chart.html" ?>
                        </div>
                    </div>
                </div>
                <div class="report-content" style="margin-top: 10px; page-break-before:always">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-7 col-lg-8">
                            <div class="col-xs-3 col-md-3 col-sm-3 col-lg-2"><img src="images/rewards-recognition-01.png" class="rel-img" alt="logo"></div>
                            <div class="col-xs-4 col-md-7 col-sm-4 col-lg-4">
                                <h2 style="margin: 1px 0 0 0;">Reconhecimento</h2>
                            </div>
                            <div class="col-xs-6 col-sm-9 col-lg-offset-1 col-lg-4"><img src="./images/P_RR_CM_slide.png" alt="Reconhecimento" style="margin-top: 10px;">
                                <!--<canvas id="P_RR_CM_slide" style="margin-top: 10px;" width="200" height="15"></canvas>-->
                            </div>
                            <div class="col-xs-12 col-lg-9" style="margin-top: 0px;">
                                <h4>Minhas conversas com outros pais sobre a escola são quase sempre positivas</h4>
                            </div>
                            <div class="col-xs-12">
                                <p>Esse item é uma indicação da proporção entre interações positivas e negativas entre pais / responsáveis. Interações positivas criam o contexto para apoio da comunidade.</p>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
                            <?php include "./html/P_RR_CM_chart.html" ?>
                        </div>
                    </div>
                </div>
                <div class="report-content" style="margin-top: 10px;">
                    <div class="row row-gray" style="padding: 10px 0;">
                        <div class="col-xs-6 col-sm-6 col-md-7 col-lg-8">
                            <div class="col-xs-3 col-md-3 col-sm-3 col-lg-2"><img src="images/positive-relations-01.png" class="rel-img" alt="logo"></div>
                            <div class="col-xs-4 col-md-7 col-sm-4 col-lg-4">
                                <h2 style="margin: 1px 0 0 0;">Relacionamento</h2>
                            </div>
                            <div class="col-xs-6 col-sm-9 col-lg-offset-1 col-lg-4"><img src="./images/P_PR_CM_slide.png" alt="Relacionamento" style="margin-top: 10px;">
                                <!--<canvas id="P_PR_CM_slide" style="margin-top: 10px;" width="200" height="15"></canvas>-->
                            </div>
                            <div class="col-xs-12 col-lg-9" style="margin-top: 0px;">
                                <h4>As atividades que acontecem na escola sempre tem bastante gente</h4>
                            </div>
                            <div class="col-xs-12">
                                <p>O apoio da comunidade às atividades organizadas pela escola, como reuniões de pais, eventos, entre outros são uma medida de quanto a escola integra - se a comunidade que serve. Participação nas atividades escolares é um
                                    indicador apropriado da qualidade dos relacionamentos entre a comunidade e escola.</p>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
                            <?php include "./html/P_PR_CM_chart.html" ?>
                        </div>
                    </div>
                </div>
                <div class="report-content" style="margin-top: 10px;">
                    <div class="col-xs-12" style="margin-top:50px; margin-bottom: 75px;">
                        <div class=""><span class="col-xs-5">Cada barra de status colorida converte a distribuição das respostas em um valor/cor que prediz resultados importantes para a escola.</span>
                            <div class="color-bar">
                                <div style="height:15px;" class="red"></div>Inadequado</div>
                            <div class="color-bar">
                                <div style="height:15px;" class="yellow"></div>Mínimo</div>
                            <div class="color-bar">
                                <div style="height:15px;" class="green"></div>Bom</div>
                            <div class="color-bar">
                                <div style="height:15px;" class="purple"></div>Exemplar</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
        $('tr:nth-child(1) > td:nth-child(1)').filter(function (i,e){return $(e).html() === 'Percent';}).html('Porcentagem');
        $('tr:nth-child(2) > td:nth-child(1)').filter(function (i,e){return $(e).html() === 'Number';}).html('Valores');
        $('svg > g > text').filter(function (i,e){return $(e).html() === 'No data available';}).html('Sem valores')
        $('svg > g > g.x.axis > g > text').filter(function (i,e) { return $(e).text() === 'SD';}).text('DC')
        $('svg > g > g.x.axis > g > text').filter(function (i,e) { return $(e).text() === 'A';}).text('C')
        $('svg > g > g.x.axis > g > text').filter(function (i,e) { return $(e).text() === 'SA';}).text('CC')
    </script>
</body>
</html>
