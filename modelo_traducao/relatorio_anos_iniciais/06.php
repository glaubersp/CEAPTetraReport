<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Relatório Tetra Analytix</title>
    <link rel="shortcut icon" href="images/favicon.png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="css/tetra.css">
    <link rel="stylesheet" href="css/ceap.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-12 report-view">
                <div class="report-header text-center">
                    <h1><?php include "./html/school_name.html" ?></h1>
                    <p class="school-level"><?php include "./html/grade_name.html" ?></p>
                    <h3>Habilidades Acadêmicas</h3>
                    <h4>Resposta dos Alunos</h4>
                </div>
                <div class="report-content" style="margin-top: 10px;">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-7 col-lg-8">
                            <div class="col-xs-3 col-md-3 col-sm-3 col-lg-2"><img src="images/clear-expectations-01.png" class="rel-img" alt="logo"></div>
                            <div class="col-xs-4 col-md-7 col-sm-4 col-lg-4">
                                <h2 style="margin: 1px 0 0 0;">Expectativas</h2>
                            </div>
                            <div class="col-xs-6 col-sm-9 col-lg-offset-1 col-lg-4"><img src="./images/S_CE_AC_slide.png" alt="Expectativas - Habilidades Acadêmicas" style="margin-top: 10px;">
                                <!--<canvas id="S_CE_AC_slide" style="margin-top: 10px;" width="200" height="15"></canvas>!-->
                            </div>
                            <div class="col-xs-12 col-lg-9" style="margin-top: 0px;" ng-hide="false">
                                <h4>Muitas vezes eu não sei o que fazer na aula</h4>
                            </div>
                            <div class="col-xs-12">
                                <p>Expectativas claras abrem caminho para o desenvolvimento das habilidades acadêmicas. Quando
                                    os alunos relatam confusão, é comum que não participem das atividades e tarefas. É importante
                                    garantir que os alunos tenham tanto o conhecimento básico necessário para que possam
                                    engajar nas tarefas, bem como clareza no seu entendimento das orientações, seja de forma
                                    escrita ou falada.</p>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
                            <?php include "./html/S_CE_AC_chart.html" ?>
                        </div>
                    </div>
                </div>
                <div class="report-content" style="margin-top: 10px;">
                    <div class="row row-gray" style="padding: 10px 0;">
                        <div class="col-xs-6 col-sm-6 col-md-7 col-lg-8">
                            <div class="col-xs-3 col-md-3 col-sm-3 col-lg-2"><img src="images/skill-building-01.png" class="rel-img" alt="logo"></div>
                            <div class="col-xs-4 col-md-7 col-sm-4 col-lg-4">
                                <h2 style="margin: 1px 0 0 0;">Habilidades</h2>
                            </div>
                            <div class="col-xs-6 col-sm-9 col-lg-offset-1 col-lg-4"><img src="./images/S_SB_AC_slide.png" alt="Habilidades - Habilidades Acadêmicas" style="margin-top: 10px;">
                                <!--<canvas id="S_SB_AC_slide" style="margin-top: 10px;" width="200" height="15"></canvas>-->
                            </div>
                            <div class="col-xs-12 col-lg-9" style="margin-top: 0px;" ng-hide="false">
                                <h4>Eu leio bastante na escola</h4>
                            </div>
                            <div class="col-xs-12">
                                <p>Habilidades acadêmicas são construídas quando providenciamos muitas oportunidades para a
                                    prática, com retorno(feedback) que seja específico e dado no momento certo. Tendo em
                                    vista que a alfabetização é aplicada em qualquer área do conhecimento, a leitura é um
                                    bom indicador de habilidades acadêmicas.</p>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
                            <?php include "./html/S_SB_AC_chart.html" ?>
                        </div>
                    </div>
                </div>
                <div class="report-content" style="margin-top: 10px; page-break-before:always">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-7 col-lg-8">
                            <div class="col-xs-3 col-md-3 col-sm-3 col-lg-2"><img src="images/rewards-recognition-01.png" class="rel-img" alt="logo"></div>
                            <div class="col-xs-4 col-md-7 col-sm-4 col-lg-4">
                                <h2 style="margin: 1px 0 0 0;">Reconhecimento</h2>
                            </div>
                            <div class="col-xs-6 col-sm-9 col-lg-offset-1 col-lg-4"><img src="./images/S_RR_AC_slide.png" alt="Reconhecimento - Habilidades Acadêmicas">
                                <!--<canvas id="S_RR_AC_slide" style="margin-top: 10px;" width="200" height="15">--></canvas>
                            </div>
                            <div class="col-xs-12 col-lg-9" style="margin-top: 0px;" ng-hide="false">
                                <h4>Todo dia, a professora percebe quando eu faço um bom trabalho</h4>
                            </div>
                            <div class="col-xs-12 ng-hide" ng-show="false">
                                <p>É essencial notar os esforços dos alunos na medida em que buscam atingir as expectativas
                                    acadêmicas. Professores que pessoalmente reconhecem os esforços de seus alunos, e criam
                                    métodos para reconhecer o crescimento de cada aluno, têm alunos mais engajados e salas
                                    de aula produtivas.</p>
                            </div>
                            <div class="col-xs-12" ng-hide="false">
                                <p>Reconhecer os esforços de alunos na medida em que tentam atingir as expectativas é essencial
                                    para que haja progresso contínuo. Professores devem, de maneira diligente, reconhecer
                                    os esforços que vão em direção aos objetivos traçados.
                                </p>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
                            <?php include "./html/S_RR_AC_chart.html" ?>
                        </div>
                    </div>
                </div>
                <div class="report-content" style="margin-top: 10px;">
                    <div class="row row-gray" style="padding: 10px 0;">
                        <div class="col-xs-6 col-sm-6 col-md-7 col-lg-8">
                            <div class="col-xs-3 col-md-3 col-sm-3 col-lg-2"><img src="images/positive-relations-01.png" class="rel-img" alt="logo"></div>
                            <div class="col-xs-4 col-md-7 col-sm-4 col-lg-4">
                                <h2 style="margin: 1px 0 0 0;">Relacionamento</h2>
                            </div>
                            <div class="col-xs-6 col-sm-9 col-lg-offset-1 col-lg-4"><img src="./images/S_PR_AC_slide.png" alt="Relacionamento - Habilidades Acadêmicas">
                                <!--<canvas id="S_PR_AC_slide" style="margin-top: 10px;" width="200" height="15"></canvas>-->
                            </div>
                            <div class="col-xs-12 col-lg-9" style="margin-top: 0px;" ng-hide="false">
                                <h4>Minha professora se importa comigo</h4>
                            </div>
                            <div class="col-xs-12 ng-hide" ng-show="false">
                                <p>Relações baseadas em confiança e estima entre alunos e professores ajudam a fazer com que
                                    as expectativas sejam claras, dão valor ao reconhecimento oferecido, e encorajam um alto
                                    nível de engajamento por parte dos alunos.</p>
                            </div>
                            <div class="col-xs-12" ng-hide="false">
                                <p>Relacionamentos entre professores e alunos que são baseados em confiança e estima ajudam
                                    a fazer com que as expectativas sejam claras, e dão maior valor ao retorno(feedback).
                                    Um relacionamento positivo gera uma percepção de valor para cada tarefa e atividade.</p>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
                            <?php include "./html/S_PR_AC_chart.html" ?>
                        </div>
                    </div>
                </div>
                <div class="report-content" style="margin-top: 10px;">
                    <div class="col-xs-12" style="margin-bottom: 75px;">
                        <div class=""><span class="col-xs-5">Cada barra de status colorida converte a distribuição das respostas em um valor/cor que prediz resultados importantes para a escola.</span>
                            <div class="color-bar">
                                <div style="height:15px;" class="red"></div>Inadequado</div>
                            <div class="color-bar">
                                <div style="height:15px;" class="yellow"></div>Mínimo</div>
                            <div class="color-bar">
                                <div style="height:15px;" class="green"></div>Bom</div>
                            <div class="color-bar">
                                <div style="height:15px;" class="purple"></div>Exemplar</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
        $('tr:nth-child(1) > td:nth-child(1)').filter(function (i,e){return $(e).html() === 'Percent';}).html('Porcentagem');
        $('tr:nth-child(2) > td:nth-child(1)').filter(function (i,e){return $(e).html() === 'Number';}).html('Valores');
        $('svg > g > text').filter(function (i,e){return $(e).html() === 'No data available';}).html('Sem valores')
        $('svg > g > g.x.axis > g > text').filter(function (i,e) { return $(e).text() === 'SD';}).text('DC')
        $('svg > g > g.x.axis > g > text').filter(function (i,e) { return $(e).text() === 'A';}).text('C')
        $('svg > g > g.x.axis > g > text').filter(function (i,e) { return $(e).text() === 'SA';}).text('CC')
    </script>
</body>
</html>
