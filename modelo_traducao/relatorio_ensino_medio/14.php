<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Relatório Tetra Analytix</title>
    <link rel="shortcut icon" href="images/favicon.png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="css/tetra.css">
    <link rel="stylesheet" href="css/ceap.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-12 report-view">
                <div class="report-header text-center">
                    <h1><?php include "./html/school_name.html" ?></h1>
                    <p class="school-level"><?php include "./html/grade_name.html" ?></p>
                    <h3>Repostas Abertas</h3>
                    <h4>Repostas dos Alunos, Professores, Pais e Responsáveis</h4>
                </div>
                <div class="report-content">
                    <div class="col-xs-12" style="margin-top: 10px;">
                        <div>
                            <?php include "./html/text-student.html" ?>
                            <?php include "./html/text-teacher.html" ?>
                            <?php include "./html/text-parent.html" ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
        $('tr > th').filter(function (i,e) { return $(e).html() === 'What do you like most about this school?';}).html('O que você mais gosta nesta escola?')
        $('tr > th').filter(function (i,e) { return $(e).html() === 'What would you like to change at this school?';}).html('O que você gostaria que fosse diferente nessa escola?')
        $('tr > td').filter(function (i,e) { return $(e).html() === 'No responses found';}).html('Sem respostas')
    </script>
</body>
</html>
