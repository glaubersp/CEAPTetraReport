<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Relatório Tetra Analytix</title>
    <link rel="shortcut icon" href="images/favicon.png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="css/tetra.css">
    <link rel="stylesheet" href="css/ceap.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-12 report-view">
                <div class="report-header text-center">
                    <h1><?php include "./html/school_name.html" ?></h1>
                    <p class="school-level"><?php include "./html/grade_name.html" ?></p>
                    <h3>Segurança, Qualidade da Instrução e Liderança</h3>
                    <h4>Repostas dos Alunos, Professores, Pais e Responsáveis</h4>
                </div>
                <div class="report-content" style="margin-top: 10px;">
                    <div class="col-xs-9 col-sm-6 col-md-7 col-lg-8"><img src="images/logo.png" width="50px" alt="logo">
                        <h2 style="display: inline-block;vertical-align: middle;">Segurança</h2>
                        <p>Abaixo estão três resumos descrevendo a segurança na escola. Essa análise permite a comparação das percepções de cada grupo participante.</p>
                        <p>A percepção dos alunos no que diz respeito a segurança é essencial. Quando alunos se sentem inseguros, outras considerações ficam em segundo plano. No entanto, a sensação de segurança por parte dos alunos, não é o suficiente. Quando
                            adultos percebem que o ambiente é menos seguro, deve-se ficar atento a essas observações. Professores(as) podem estar mais atentos a questões relacionadas ao ambiente interno da escola, como a segurança do prédio e das áreas
                            comuns. Pais podem ter uma noção melhor de questões da comunidade, como crimes e trajetos seguros. Quando alunos indicam um ambiente menos seguros que adultos, a questão do bullying pode estar passando desapercebida.</p>
                    </div>
                    <div class="col-xs-3 col-sm-6 col-md-5 col-lg-4"><br>
                        <p></p>
                        <h4>Segurança</h4>
                        <p></p><img src="./images/S_1_gold_slide.png" alt="Alunos">
                        <!--<canvas id="S_1_gold_slide" width="200" height="15"></canvas>-->
                        <p>Alunos</p><img src="./images/T_1_gold_slide.png" alt="Professores">
                        <!--<canvas id="T_1_gold_slide" width="200" height="15"></canvas>-->
                        <p>Professores</p><img src="./images/P_1_gold_slide.png" alt="Pais">
                        <!--<canvas id="P_1_gold_slide" width="200" height="15"></canvas>-->
                        <p>Pais</p>
                    </div>
                </div>
                <div class="report-content" style="margin-top: 10px;clear: both">
                    <div class="col-xs-9 col-sm-6 col-md-7 col-lg-8"><img src="images/logo.png" width="50px" alt="logo">
                        <h2 style="display: inline-block;vertical-align: middle;">Qualidade da Instrução</h2>
                        <p>Abaixo estão três resumos descrevendo a qualidade da instrução na escola. Essa análise permite a comparação das percepções de cada grupo participante.</p>
                        <p>A percepção dos alunos quanto a qualidade da instrução indica, potencialmente, níveis de frustração com o ambiente de ensino ou das práticas pedagógicas. Pode também indicar sua satisfação com seus professores. As percepções de
                            pais indicam o grau de familiaridade dos mesmos com o que acontece em sala de aula. Diferenças substanciais entre as duas percepções (alunos e pais) indica lacunas de comunicação da escola com os pais. A percepção de professores(as)
                            indica sua percepção de sucesso na instrução. Quando diferem da percepção dos alunos, os professores(as) podem não notar limitações na sua própria prática.</p>
                    </div>
                    <div class="col-xs-3 col-sm-6 col-md-5 col-lg-4"><br>
                        <p></p>
                        <h4>Qualidade da Instrução</h4>
                        <p></p><img src="./images/S_2_gold_slide.png" alt="Alunos">
                        <!--<canvas id="S_2_gold_slide" width="200" height="15"></canvas>-->
                        <p>Alunos</p><img src="./images/T_2_gold_slide.png" alt="Professores">
                        <!--<canvas id="T_2_gold_slide" width="200" height="15"></canvas>-->
                        <p>Professores</p><img src="./images/P_2_gold_slide.png" alt="Pais">
                        <!--<canvas id="P_2_gold_slide" width="200" height="15"></canvas>-->
                        <p>Pais</p>
                    </div>
                </div>
                <div class="report-content" style="margin-top: 10px;clear: both; page-break-before:always">
                    <div class="col-xs-9 col-sm-6 col-md-7 col-lg-8"><img src="images/logo.png" width="50px" alt="logo">
                        <h2 style="display: inline-block;vertical-align: middle;">Liderança</h2>
                        <p>Abaixo estão três resumos descrevendo a liderança instrucional na escola. Essa análise permite a comparação das percepções de cada grupo participante.</p>
                        <p>A percepção dos professores(as) sobre liderança indica a confiança que os mesmos tem no papel da gestão para fomentar práticas de sucesso na escola e na sala de aula. Quando outros grupos indicam um valor menor para essa liderança
                            instrucional do que a percepção dos professores, há evidência que a liderança não atinge esses grupos. Quando outros grupos indicam uma liderança mais forte do que a indicada pelos professores(as), esses grupos podem não perceber
                            os problemas que professores(as) tem com a gestão escolar. É importante que a gestão tenha a confiança dos professores(as) e que outros grupos notem essa confiança.</p>
                        <p>Abaixo estão três resumos descrevendo a liderança organizacional na escola. Essa análise permite a comparação das percepções de cada grupo participante.</p>
                        <p>A percepção dos professores(as) sobre liderança organizacional indica o grau de apoio da gestão para a colaboração entre professores(as). Quando outros grupos indicam um valor menor para essa liderança instrucional do que a percepção
                            dos professores, há evidência que a colaboração não atinge esses grupos. Quando outros grupos indicam uma liderança mais forte do que a indicada pelos professores(as), esses grupos podem não perceber os problemas de relacionamento
                            entre os professores(as). É importante que a gestão trabalhe para criar um ambiente de colaboração e confiança tendo em mente os objetivos da escola. Novamente, é importante que outros grupos notem evidências dessas práticas.</p>
                    </div>
                    <div class="col-xs-3 col-sm-6 col-md-5 col-lg-4"><br>
                        <p></p>
                        <h4>Liderança Instrucional</h4>
                        <p></p><img src="./images/S_3_gold_slide.png" alt="Alunos">
                        <!--<canvas id="S_3_gold_slide" width="200" height="15"></canvas>-->
                        <p>Alunos</p><img src="./images/T_3_gold_slide.png" alt="Professores">
                        <!--<canvas id="T_3_gold_slide" width="200" height="15"></canvas>-->
                        <p>Professores</p><img src="./images/P_3_gold_slide.png" alt="Pais">
                        <!--<canvas id="P_3_gold_slide" width="200" height="15"></canvas>-->
                        <p>Pais</p><br>
                        <p></p>
                        <h4>Liderança Organizacional</h4>
                        <p></p><img src="./images/S_4_gold_slide.png" alt="Alunos">
                        <!--<canvas id="S_4_gold_slide" width="200" height="15"></canvas>-->
                        <p>Alunos</p><img src="./images/T_4_gold_slide.png" alt="Professores">
                        <!--<canvas id="T_4_gold_slide" width="200" height="15"></canvas>-->
                        <p>Professores</p><img src="./images/P_4_gold_slide.png" alt="Pais">
                        <!--<canvas id="P_4_gold_slide" width="200" height="15"></canvas>-->
                        <p>Pais</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
