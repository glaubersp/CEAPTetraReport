<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Relatório Tetra Analytix</title>
    <link rel="shortcut icon" href="images/favicon.png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="css/tetra.css">
    <link rel="stylesheet" href="css/ceap.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-12 report-view">
                <div class="report-header text-center">
                    <h1><?php include "./html/school_name.html" ?></h1>
                    <p class="school-level"><?php include "./html/grade_name.html" ?></p>
                    <h3>Colaboração</h3>
                    <h4>Resposta dos Professores</h4>
                </div>
                <div class="report-content" style="margin-top: 10px;">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-7 col-lg-8">
                            <div class="col-xs-3 col-md-3 col-sm-3 col-lg-2"><img src="images/clear-expectations-01.png" class="rel-img" alt="logo"></div>
                            <div class="col-xs-4 col-md-7 col-sm-4 col-lg-4">
                                <h2 style="margin: 1px 0 0 0;">Expectativas</h2>
                            </div>
                            <div class="col-xs-6 col-sm-9 col-lg-offset-1 col-lg-4"><img src="./images/T_CE_CL_slide.png" alt="Expectativas" style="margin-top: 10px;">
                                <!--<canvas id="T_CE_CL_slide" style="margin-top: 10px;" width="200" height="15"></canvas>-->
                            </div>
                            <div class="col-xs-12 col-lg-9" style="margin-top: 0px;">
                                <h4>A direção da escola comunica claramente o que espera quanto a colaboração entre professores</h4>
                            </div>
                            <div class="col-xs-12">
                                <p>Estabelecer momentos para a colaboração não é o suficiente. Muitas vezes, expectativas e regras para a colaboração não são explícitas. Gestores devem providenciar recomendações e critérios claros para os processos e resultados
                                    esperados na condução de atividades colaborativas. Isso ajudará professores a traduzir as atividades de colaboração em melhorias mensuráveis em seu trabalho com os alunos.</p>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
                            <?php include "./html/T_CE_CL_chart.html" ?>
                        </div>
                    </div>
                </div>
                <div class="report-content" style="margin-top: 10px;">
                    <div class="row row-gray" style="padding: 10px 0;">
                        <div class="col-xs-6 col-sm-6 col-md-7 col-lg-8">
                            <div class="col-xs-3 col-md-3 col-sm-3 col-lg-2"><img src="images/skill-building-01.png" class="rel-img" alt="logo"></div>
                            <div class="col-xs-4 col-md-7 col-sm-4 col-lg-4">
                                <h2 style="margin: 1px 0 0 0;">Habilidades</h2>
                            </div>
                            <div class="col-xs-6 col-sm-9 col-lg-offset-1 col-lg-4"><img src="./images/T_SB_CL_slide.png" alt="Habilidades" style="margin-top: 10px;">
                                <!--<canvas id="T_SB_CL_slide" style="margin-top: 10px;" width="200" height="15"></canvas>-->
                            </div>
                            <div class="col-xs-12 col-lg-9" style="margin-top: 0px;">
                                <h4>Professores nessa escola trabalham bem uns com os outros</h4>
                            </div>
                            <div class="col-xs-12">
                                <p>Colaboração efetiva precisa ser fomentada. Não assuma que professores podem desenvolver habilidades de colaboração em isolamento. Gestores devem providenciar tempo específico e formação para que a colaboração possa contribuir
                                    para os objetivos definidos pela escola.</p>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
                            <?php include "./html/T_SB_CL_chart.html" ?>
                        </div>
                    </div>
                </div>
                <div class="report-content" style="margin-top: 10px; page-break-before:always">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-7 col-lg-8">
                            <div class="col-xs-3 col-md-3 col-sm-3 col-lg-2"><img src="images/rewards-recognition-01.png" class="rel-img" alt="logo"></div>
                            <div class="col-xs-4 col-md-7 col-sm-4 col-lg-4">
                                <h2 style="margin: 1px 0 0 0;">Reconhecimento</h2>
                            </div>
                            <div class="col-xs-6 col-sm-9 col-lg-offset-1 col-lg-4"><img src="./images/T_RR_CL_slide.png" alt="Reconhecimento" style="margin-top: 10px;">
                                <!--<canvas id="T_RR_CL_slide" style="margin-top: 10px;" width="200" height="15"></canvas>-->
                            </div>
                            <div class="col-xs-12 col-lg-9" style="margin-top: 0px;">
                                <h4>Minhas interações com outros professores são quase sempre positivas</h4>
                            </div>
                            <div class="col-xs-12">
                                <p>Esse item é uma indicação da proporção entre interações positivas e negativas entre professores. Interações positivas criam um contexto para colaboração e possibilidades de orientação.</p>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
                            <?php include "./html/T_RR_CL_chart.html" ?>
                        </div>
                    </div>
                </div>
                <div class="report-content" style="margin-top: 10px;">
                    <div class="row row-gray" style="padding: 10px 0;">
                        <div class="col-xs-6 col-sm-6 col-md-7 col-lg-8">
                            <div class="col-xs-3 col-md-3 col-sm-3 col-lg-2"><img src="images/positive-relations-01.png" class="rel-img" alt="logo"></div>
                            <div class="col-xs-4 col-md-7 col-sm-4 col-lg-4">
                                <h2 style="margin: 1px 0 0 0;">Relacionamento</h2>
                            </div>
                            <div class="col-xs-6 col-sm-9 col-lg-offset-1 col-lg-4"><img src="./images/T_PR_CL_slide.png" alt="Relacionamento" style="margin-top: 10px;">
                                <!--<canvas id="T_PR_CL_slide" style="margin-top: 10px;" width="200" height="15"></canvas>-->
                            </div>
                            <div class="col-xs-12 col-lg-9" style="margin-top: 0px;">
                                <h4>Os professores quase nunca faltam às aulas</h4>
                            </div>
                            <div class="col-xs-12">
                                <p>Quando professores conseguem criar amizades com colegas na escola, eles têm vontade de vir à escola. Esse item é um indicador da qualidade desses relacionamentos.</p>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-5 col-lg-4">
                            <?php include "./html/T_PR_CL_chart.html" ?>
                        </div>
                    </div>
                </div>
                <div class="report-content" style="margin-top: 10px;">
                    <div class="col-xs-12" style="margin-top:50px; margin-bottom: 75px;">
                        <div class=""><span class="col-xs-5">Cada barra de status colorida converte a distribuição das respostas em um valor/cor que prediz resultados importantes para a escola.</span>
                            <div class="color-bar">
                                <div style="height:15px;" class="red"></div>Inadequado</div>
                            <div class="color-bar">
                                <div style="height:15px;" class="yellow"></div>Mínimo</div>
                            <div class="color-bar">
                                <div style="height:15px;" class="green"></div>Bom</div>
                            <div class="color-bar">
                                <div style="height:15px;" class="purple"></div>Exemplar</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
        $('tr:nth-child(1) > td:nth-child(1)').filter(function (i,e){return $(e).html() === 'Percent';}).html('Porcentagem');
        $('tr:nth-child(2) > td:nth-child(1)').filter(function (i,e){return $(e).html() === 'Number';}).html('Valores');
        $('svg > g > text').filter(function (i,e){return $(e).html() === 'No data available';}).html('Sem valores')
        $('svg > g > g.x.axis > g > text').filter(function (i,e) { return $(e).text() === 'SD';}).text('DC')
        $('svg > g > g.x.axis > g > text').filter(function (i,e) { return $(e).text() === 'A';}).text('C')
        $('svg > g > g.x.axis > g > text').filter(function (i,e) { return $(e).text() === 'SA';}).text('CC')
    </script>
</body>
</html>
