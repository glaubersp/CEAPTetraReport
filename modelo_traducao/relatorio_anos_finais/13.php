<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Relatório Tetra Analytix</title>
    <link rel="shortcut icon" href="images/favicon.png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="css/tetra.css">
    <link rel="stylesheet" href="css/ceap.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="container-fluid">
        <div class="row page-break">
            <div class="col-xs-12 col-md-12 report-view">
                <div class="report-header text-center">
                    <h1><?php include "./html/school_name.html" ?></h1>
                    <p class="school-level"><?php include "./html/grade_name.html" ?></p>
                    <h3>Dados Demográficos</h3>
                    <h4>Repostas dos Alunos, Professores, Pais e Responsáveis</h4>
                </div>
                <div class="report-content">
                    <div class="col-xs-12">
                        <p>Abaixo está um resumo das características demográficas dos participantes. Todos os números representam percentagens. Os alunos selecionam um "ano" e um "sexo". Todas as outras variáveis permitem a seleção de mais uma opção, e portanto,
                            podem totalizar mais de 100% .</p>
                        <?php include "./html/demo-grade.html" ?>
                        <?php include "./html/demo-ethnicity.html" ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
        $('#demo-grade > tbody > tr:nth-child(1) > th > h3').html('Ano (%)')
        $('#demo-grade > tbody > tr:nth-child(1) > td:nth-child(2)').html('Alunos')
        $('#demo-grade > tbody > tr:nth-child(1) > td:nth-child(3)').html('Professores')
        $('#demo-grade > tbody > tr:nth-child(1) > td:nth-child(4)').html('Pais')
        $('#demo-grade-2 > td:nth-child(1) > h4:nth-child(2)').html('2<sup>o</sup> ano')
        $('#demo-grade-3 > td:nth-child(1) > h4:nth-child(2)').html('3<sup>o</sup> ano')
        $('#demo-grade-4 > td:nth-child(1) > h4:nth-child(2)').html('4<sup>o</sup> ano')
        $('#demo-grade-5 > td:nth-child(1) > h4:nth-child(2)').html('5<sup>o</sup> ano')
        $('#demo-grade-6 > td:nth-child(1) > h4:nth-child(2)').html('6<sup>o</sup> ano')
        $('#demo-grade-7 > td:nth-child(1) > h4:nth-child(2)').html('7<sup>o</sup> ano')
        $('#demo-grade-8 > td:nth-child(1) > h4:nth-child(2)').html('8<sup>o</sup> ano')
        $('#demo-grade-9 > td:nth-child(1) > h4:nth-child(2)').html('9<sup>o</sup> ano')
        $('#demo-grade-10 > td:nth-child(1) > h4:nth-child(2)').html('1<sup>o</sup> ano do EM')
        $('#demo-grade-11 > td:nth-child(1) > h4:nth-child(2)').html('2<sup>o</sup> ano do EM')
        $('#demo-grade-12 > td:nth-child(1) > h4:nth-child(2)').html('3<sup>o</sup> ano do EM')
        $('#demo-ethnicity > tbody > tr:nth-child(1) > th > h3').html('Etnicidade (%)')
        $('#demo-ethnicity > tbody > tr:nth-child(1) > td:nth-child(2)').html('Alunos')
        $('#demo-ethnicity > tbody > tr:nth-child(1) > td:nth-child(3)').html('Professores')
        $('#demo-ethnicity > tbody > tr:nth-child(1) > td:nth-child(4)').html('Pais')
        $('#demo-ethnicity-ASIAN > td:nth-child(1) > h4').html('Asiático')
        $('#demo-ethnicity-PACIFIC_ISLANDER > td:nth-child(1) > h4').html('Moreno')
        $('#demo-ethnicity-BLACK > td:nth-child(1) > h4').html('Negro')
        $('#demo-ethnicity-AMERICAN_INDIAN > td:nth-child(1) > h4').html('Indígena')
        $('#demo-ethnicity-WHITE > td:nth-child(1) > h4').html('Branco')
        $('#demo-ethnicity-OTHER').css("background-color","white");
        $('#demo-ethnicity-OTHER > td:nth-child(1) > h4').html('Outro')
        $('#demo-ethnicity > tbody > tr:nth-child(10) > td:nth-child(1) > h4').html('% masculino')
        $('#demo-ethnicity-HISPANIC').remove()
    </script>
</body>
</html>
