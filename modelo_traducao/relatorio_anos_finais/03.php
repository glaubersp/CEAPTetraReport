<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Relatório Tetra Analytix</title>
    <link rel="shortcut icon" href="images/favicon.png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="css/tetra.css">
    <link rel="stylesheet" href="css/ceap.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-12 report-view">
                <div class="report-header text-center">
                    <h1>
                        <?php include "./html/school_name.html" ?>
                    </h1>
                    <p class="school-level">
                        <?php include "./html/grade_name.html" ?>
                    </p>
                    <h3>Condições para o Aprendizado</h3>
                    <h4>Respostas dos Alunos</h4>
                </div>
                <div class="report-content">
                    <div class="col-xs-12 col-sm-6 col-md-7 col-lg-6"><img src="images/logo.png" width="50px" alt="logo">
                        <h2 style="display: inline-block;vertical-align: middle;">Habilidades Acadêmicas</h2>
                        <p>O ponto focal em habilidades acadêmicas representa as estratégias professores e funcionários usam
                            para que as expectativas acadêmicas sejam claras, como criam oportunidades o desenvolvimento
                            de habilidades acadêmicas, e como reconhecem os esforços para atingir os objetivos de alto nível.
                            Também mensura os relacionamentos de confiança na escola. Esses itens foram desenvolvidos para
                            examinar as interações entre professores e alunos.</p>
                        <p>Abaixo você encontra dois resumos das condições indicadas por alunos no que tange as habilidades
                            acadêmicas. O histograma indica a percentagem de alunos que indica ter 0, 1, 2, 3 ou todas as
                            4 condições. O triângulo representa os mesmos dados de uma maneira alternativa, indicando a percentagem
                            relativa de alunos que reportaram 0 ou 1 condição e que podem precisar de atenção especial e
                            apoio individualizado(vermelho); 2 condições, ou os que podem demandar apoio em grupo, de maneira
                            sistemática(amarelo); e 3 - 4 condições, os que se beneficiariam de programas de sensibilização
                            e apoio de aplicação universal(verde / roxo).</p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5 col-lg-6 center-image"><br>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <?php include "./html/S_AC_chart.html" ?>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <img src="./images/S_AC_triangle.png" alt="Habilidades Acadêmicas">
                            <br>
                            <span style="font-size: 10px;">Sua Escola</span>
                            <span style="font-size: 10px;margin-left: 65px;">Padrão</span>
                            <p>Habilidades Acadêmicas</p>
                        </div>
                    </div>
                </div>
                <div class="report-content" style="page-break-before:always">
                    <div class="col-xs-12 col-sm-6 col-md-7 col-lg-6"><img src="images/logo.png" width="50px" alt="logo">
                        <h2 style="display: inline-block;vertical-align: middle;">Habilidades Interpessoais</h2>
                        <p>O ponto focal em habilidades interpessoais representa as condições presentes na escola que servem
                            para estabelecer e dar impulso aos comportamentos definidos como apropriados. Esses indicadores
                            estão ligados às expectativa sociais, os esforços sistemáticos feitos para ajudar no desenvolvimento
                            dessas habilidades, os esforços de alunos em reconhecer e encorajar uns aos outros bem como relações
                            apropriadas e positivas entre os alunos.</p>
                        <p>Abaixo você encontra dois resumos das condições indicadas por alunos no que tange as habilidades
                            interpessoais. O histograma indica a percentagem de alunos que indica ter 0, 1, 2, 3 ou todas
                            as 4 condições. O triângulo representa os mesmos dados de uma maneira alternativa, indicando
                            a percentagem relativa de alunos que reportaram 0 ou 1 condição e que podem precisar de atenção
                            especial e apoio individualizado(vermelho); 2 condições, ou os que podem demandar apoio em grupo,
                            de maneira sistemática(amarelo); e 3 - 4 condições, os que se beneficiariam de programas de sensibilização
                            e apoio de aplicação universal(verde / roxo).</p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5 col-lg-6 center-image"><br>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <?php include "./html/S_IP_chart.html" ?>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <img src="./images/S_IP_triangle.png" alt="Habilidades Interpessoais">
                            <br>
                            <span style="font-size: 10px;">Sua Escola</span>
                            <span style="font-size: 10px;margin-left: 65px;">Padrão</span>
                            <p>Habilidades Interpessoais</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
        $('tr:nth-child(1) > td:nth-child(1)').filter(function (i,e){return $(e).html() === 'Percent';}).html('Porcentagem');
        $('tr:nth-child(2) > td:nth-child(1)').filter(function (i,e){return $(e).html() === 'Number';}).html('Valores');
        $('svg > g > text').filter(function (i,e){return $(e).html() === 'No data available';}).html('Sem valores')
        $('svg > g > g.x.axis > g > text').filter(function (i,e) { return $(e).text() === 'SD';}).text('DC')
        $('svg > g > g.x.axis > g > text').filter(function (i,e) { return $(e).text() === 'A';}).text('C')
        $('svg > g > g.x.axis > g > text').filter(function (i,e) { return $(e).text() === 'SA';}).text('CC')
    </script>
</body>
</html>