<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Relatório Tetra Analytix</title>
    <link rel="shortcut icon" href="images/favicon.png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="css/tetra.css">
    <link rel="stylesheet" href="css/ceap.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="container-fluid">
        <div class="row page-break">
            <div class="col-xs-12 col-md-12 report-view">
                <div class="report-header text-center">
                    <h1><?php include "./html/school_name.html" ?></h1>
                    <p class="school-level"><?php include "./html/grade_name.html" ?></p>
                    <h3>Resumo Executivo CEAP</h3>
                </div>
                <div class="report-content">
                    <div class="col-xs-12">
                        <!-- <img class="bullseye" src="images/bullseye.png" width="70%" style="max-width: 600px;" alt="conditions bullseye"> -->
                        <p>O CEAP examina condições críticas em três contextos: a aula (Condições para o Aprendizado), a escola (Condições para o Ensino) e a casa ou comunidade (Condições para o Apoio da Comunidade). Esses três contextos são divididos em
                            enfoques (habilidades acadêmicas e sociais, ensino e colaboração, e envolvimento dos pais/responsáveis e apoio da comunidade) que providenciam maior profundidade na análise das funções desses contextos. Cada enfoque é examinado
                            através de quatro condições:</p>
                        <p><strong>Expectativas.</strong> Comunicar expectativas elevadas de maneira clara é um primeiro passo para fazer com que qualquer atividade de ensino, apoio e atividades de orientação sejam eficazes. Depende da definição de critérios
                            de sucesso, ações necessárias, resultados esperados e deve incluir uma justificativa para cada ação. O uso de uma linguagem comum na escola leva ao entendimento comum sobre expectativas, o que por sua vez resulta em sucesso
                            compartilhado.
                        </p>
                        <p><strong>Habilidades.</strong> A ênfase no aquisição e fortalecimento de habilidades resulta em aprendizado. A melhor maneira de se desenvolver habilidades é através da prática. Alunos, professores e membros da comunidade devem
                            participar ativamente de oportunidades de aprendizado nas quais possam engajar de maneira ativa e com expectativas crescentes.</p>
                        <p><strong>Reconhecimento.</strong> Indivíduos devem ser reconhecidos quando se esforçam para atingir as expectativas de alto nível estabelecidas pela escola.O reconhecimento deve ser feito no momento correto e deve ser descritivo,
                            incluindo uma explicação do porque o que está sendo reconhecido é importante.</p>
                        <p><strong>Relacionamentos.</strong> Estabelecer e manter relacionamentos baseados em confiança, respeito e estima aumenta a motivação e cria uma fundação sólida, particularmente para o ensino de habilidades e conceitos complexos.
                            Bons relacionamentos servem para apoiar àqueles que podem se sentir sobrecarregados ou sem norte.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
