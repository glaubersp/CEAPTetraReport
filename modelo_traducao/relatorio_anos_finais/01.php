<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Relatório Tetra Analytix</title>
    <link rel="shortcut icon" href="images/favicon.png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="css/tetra.css">
    <link rel="stylesheet" href="css/ceap.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-12 report-view">
                <div class="report-header text-center">
                    <h1>
                        <?php include "./html/school_name.html" ?>
                    </h1>
                    <p class="school-level">
                        <?php include "./html/grade_name.html" ?>
                    </p>
                    <h3>Resumo</h3>
                </div>
                <div class="report-content">
                    <div class="col-xs-12">
                        <!--<p>Below is a summary of your Snapshot results from:</p>-->
                        <h3 style="text-align: center;margin-top: 30px">
                            <?php include "./html/report_date.html" ?>
                        </h3>
                        <p style="margin-top: 30px">Abaixo está um resumo dos resultados dos questionários CEAP. A linha mais acima mostra o número de
                            alunos, professores e pais/responsáveis que responderam ao questionário. A coloração indica a
                            representatividade de cada grupo: verde indica um número adequado de respostas e amarelo indica
                            um número suficiente. Vermelho indica um número insuficiente de participantes, o que aponta para
                            a necessidade de avaliar os resultados com cautela.</p>
                        <div id="chart-table">
                            <table>
                                <tbody>
                                    <tr>
                                        <td style="col-xs-3 col-xs-offset-3">Adequação das Respostas</td>
                                        <th style="col-xs-3 col-xs-offset-3" class="center">
                                            <h3>Alunos</h3>
                                        </th>
                                        <th style="col-xs-3 col-xs-offset-3" class="center dashed-border">
                                            <h3>Professores</h3>
                                        </th>
                                        <th style="col-xs-3 col-xs-offset-3" class="center">
                                            <h3>Pais</h3>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td class="col-xs-2 col-xs-offset-2">
                                            <div style="margin-top: 10px;">
                                                <div class="color-box" style="background-color: #b2d241;"></div>Bom</div>
                                            <div style="margin-top: 10px;">
                                                <div class="color-box" style="background-color: #f9ee22;"></div>Suficiente</div>
                                            <div style="margin-top: 10px;">
                                                <div class="color-box" style="background-color: #ef5356;"></div>Insuficiente</div>
                                        </td>
                                        <td class="col-xs-2 col-xs-offset-2">
                                            <?php include "./html/num-students.html" ?>
                                        </td>
                                        <td class="col-xs-2 col-xs-offset-2 dashed-border">
                                            <?php include "./html/num-teachers.html" ?>
                                        </td>
                                        <td class="col-xs-2 col-xs-offset-2">
                                            <?php include "./html/num-parents.html" ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <p>Os outros números na tabela são as percentagens de respondentes que "concordam" ou "concordam completamente"
                            com a existência dessa condição para esse ponto focal. Maiores esclarecimentos sobre cada ponto
                            focal estão no Resumo Executivo CEAP. Os dados dos alunos são utilizados para resumir as condições
                            para o aprendizado de habilidades acadêmicas e interpessoais. Dados dos professores são utilizados
                            para resumir as condições para ensino e colaboração. Os dados dos pais/responsáveis são utilizados
                            para resumir as condições para envolvimento dos pais/responsáveis e apoio da comunidade. Em todos,
                            um valor maior é melhor.
                            <u>Valores acima de 80% indicam práticas escolares efetivas e um número abaixo de 60% indica uma
                                prioridade.
                            </u>
                        </p>
                    </div>
                </div>
                <div class="report-content">
                    <div class="col-xs-12" id="summary-table">
                        <table>
                            <tbody>
                                <tr>
                                    <td style="width: 200px;padding-left: 20px;" class="spacer">&nbsp;</td>
                                    <td style="width: 300px;" class="center">Habilidades Acadêmicas</td>
                                    <td style="width: 300px;" class="center">Instrução</td>
                                    <td style="width: 300px;" class="center">Envolvimento dos Pais/Responsáveis</td>
                                </tr>
                                <tr class="row-gray">
                                    <td style="padding-left: 10px;">
                                        <h3>Expectativas</h3>
                                    </td>
                                    <td class="center">
                                        <h3 id="S_CE_AC">
                                            <?php include "./html/num-S_CE_AC.html" ?>
                                        </h3>
                                    </td>
                                    <td class="center dashed-border">
                                        <h3 id="T_CE_IT">
                                            <?php include "./html/num-T_CE_IT.html" ?>
                                        </h3>
                                    </td>
                                    <td class="center">
                                        <h3 id="P_CE_IV">
                                            <?php include "./html/num-P_CE_IV.html" ?>
                                        </h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 10px">
                                        <h3>Habilidades</h3>
                                    </td>
                                    <td class="center">
                                        <h3 id="S_SB_AC">
                                            <?php include "./html/num-S_SB_AC.html" ?>
                                        </h3>
                                    </td>
                                    <td class="center dashed-border">
                                        <h3 id="T_SB_IT">
                                            <?php include "./html/num-T_SB_IT.html" ?>
                                        </h3>
                                    </td>
                                    <td class="center">
                                        <h3 id="P_SB_IV">
                                            <?php include "./html/num-P_SB_IV.html" ?>
                                        </h3>
                                    </td>
                                </tr>
                                <tr class="row-gray">
                                    <td style="padding-left: 10px">
                                        <h3>Reconhecimento</h3>
                                    </td>
                                    <td class="center">
                                        <h3 id="S_RR_AC">
                                            <?php include "./html/num-S_RR_AC.html" ?>
                                        </h3>
                                    </td>
                                    <td class="center dashed-border">
                                        <h3 id="T_RR_IT">
                                            <?php include "./html/num-T_RR_IT.html" ?>
                                        </h3>
                                    </td>
                                    <td class="center">
                                        <h3 id="P_RR_IV">
                                            <?php include "./html/num-P_RR_IV.html" ?>
                                        </h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 10px;">
                                        <h3>Relacionamento</h3>
                                    </td>
                                    <td class="center">
                                        <h3 id="S_PR_AC">
                                            <?php include "./html/num-S_PR_AC.html" ?>
                                        </h3>
                                    </td>
                                    <td class="center dashed-border">
                                        <h3 id="T_PR_IT">
                                            <?php include "./html/num-T_PR_IT.html" ?>
                                        </h3>
                                    </td>
                                    <td class="center">
                                        <h3 id="P_PR_IV">
                                            <?php include "./html/num-P_PR_IV.html" ?>
                                        </h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="spacer">&nbsp;</td>
                                    <td class="center spacer">Habilidades Interpessoais</td>
                                    <td class="center dashed-border spacer">Colaboração</td>
                                    <td class="center spacer">Apoio da Comunidade</td>
                                </tr>
                                <tr class="row-gray">
                                    <td style="padding-left: 10px;">
                                        <h3>Expectativas</h3>
                                    </td>
                                    <td class="center">
                                        <h3 id="S_CE_IP">
                                            <?php include "./html/num-S_CE_IP.html" ?>
                                        </h3>
                                    </td>
                                    <td class="center dashed-border">
                                        <h3 id="T_CE_CL">
                                            <?php include "./html/num-T_CE_CL.html" ?>
                                        </h3>
                                    </td>
                                    <td class="center">
                                        <h3 id="P_CE_CM">
                                            <?php include "./html/num-P_CE_CM.html" ?>
                                        </h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 10px;">
                                        <h3>Habilidades</h3>
                                    </td>
                                    <td class="center">
                                        <h3 id="S_SB_IP">
                                            <?php include "./html/num-S_SB_IP.html" ?>
                                        </h3>
                                    </td>
                                    <td class="center dashed-border">
                                        <h3 id="T_SB_CL">
                                            <?php include "./html/num-T_SB_CL.html" ?>
                                        </h3>
                                    </td>
                                    <td class="center">
                                        <h3 id="P_SB_CM">
                                            <?php include "./html/num-P_SB_CM.html" ?>
                                        </h3>
                                    </td>
                                </tr>
                                <tr class="row-gray">
                                    <td style="padding-left: 10px;">
                                        <h3>Reconhecimento</h3>
                                    </td>
                                    <td class="center">
                                        <h3 id="S_RR_IP">
                                            <?php include "./html/num-S_RR_IP.html" ?>
                                        </h3>
                                    </td>
                                    <td class="center dashed-border">
                                        <h3 id="T_RR_CL">
                                            <?php include "./html/num-T_RR_CL.html" ?>
                                        </h3>
                                    </td>
                                    <td class="center">
                                        <h3 id="P_RR_CM">
                                            <?php include "./html/num-P_RR_CM.html" ?>
                                        </h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 10px;">
                                        <h3>Relacionamento</h3>
                                    </td>
                                    <td class="center">
                                        <h3 id="S_PR_IP">
                                            <?php include "./html/num-S_PR_IP.html" ?>
                                        </h3>
                                    </td>
                                    <td class="center dashed-border">
                                        <h3 id="T_PR_CL">
                                            <?php include "./html/num-T_PR_CL.html" ?>
                                        </h3>
                                    </td>
                                    <td class="center">
                                        <h3 id="P_PR_CM">
                                            <?php include "./html/num-P_PR_CM.html" ?>
                                        </h3>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
</body>
</html>