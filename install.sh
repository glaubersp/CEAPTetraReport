#!/bin/bash
ROOT=$(pwd)

# Para instalar o NodeJS diretamente do PKG, descomente a linha abaixo e comente as linhas de instalação do Homebrew e do NodeJS via Homebrew.
#curl "https://nodejs.org/dist/latest/node-${VERSION:-$(wget -qO- https://nodejs.org/dist/latest/ | sed -nE 's|.*>node-(.*)\.pkg</a>.*|\1|p')}.pkg" > "$HOME/Downloads/node-latest.pkg" && sudo installer -store -pkg "$HOME/Downloads/node-latest.pkg" -target "/"

# Instalação do Homebrew e do NodeJS via Homebrew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew update && brew install node

# Edite o nome do arquivo do PRINCE na variável abaixo, de acordo com a versão existente na página https://www.princexml.com/download/
PRINCE_FILE_NAME=prince-12.2.1-macosx

# Baixa e instala o Prince
curl https://www.princexml.com/download/$PRINCE_FILE_NAME.tar.gz --output $ROOT/$PRINCE_FILE_NAME.tar.gz
cd $ROOT
tar xzf $PRINCE_FILE_NAME.tar.gz
cd $PRINCE_FILE_NAME
sudo ./install.sh
cd $ROOT
rm -fr $ROOT/$PRINCE_FILE_NAME $ROOT/$PRINCE_FILE_NAME.tar.gz

# Instala gerador de relatório e suas dependências
npm install

# Executa a geração de relatórios.
./gerar_relatorio.sh
