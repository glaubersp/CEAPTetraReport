# Relatorio Tetra Analytics

## Dependências

- Linux ou MacOS
- NodeJS (https://nodejs.org/en/)
- Prince PDF Library (https://www.princexml.com)

## Instalação

1. Instalar NodeJS a partir da página https://nodejs.org/en/download/.
2. Instalar o Prince PDF a partir da página https://www.princexml.com/download/.
3. Na pasta do projeto, executar no terminal:

   npm install

## Execução

1. Editar o arquivo `gerar_relatorio.sh` e:
   - alterar usuário e senha de acesso ao TetraAnalytix.
   - alterar o caminho para o programa Prince, caso não seja o padrão.
   - alterar o caminho para o programa CasperJS, caso não seja o padrão.
2. Na pasta do projeto, executar no terminal:

   ./gerar_relatorio.sh

## Execução ALL-IN-ONE

O script `install.sh` é uma tentativa de automatização dos passos anteriores e pode ser utilizado após a edição do arquivo `gerar_relatorio.sh` indicada anteriormente.
Basta executar no terminal:

    chmod +x install.sh
    ./install.sh
