#!/bin/bash

###
### EDITE AS VARIÁVEIS A SEGUIR
###

# Defina as senhas de acesso ao sistema do TetraAnalytix
USUARIO="withomaz@yahoo.com.br" #"usuario@tetraanalytix.com"
SENHA="cps#000" #"senha_do_usuario"

# Edite o caminho para a ferramenta prince, caso necessário.
PRINCE="/usr/local/bin/prince"

# Edite o caminho para a ferramenta casperjs, se necessário.
CASPERJS_PATH="node_modules/.bin/casperjs"

###
###
### NÃO EDITAR A PARTIR DAQUI
###
###

if [[ ! -e "$CASPERJS_PATH" ]]; then
    echo "Instale as dependências com a ferramenta NodeJS (www.nodejs.com)."
    echo "Execute \$ npm install antes de executar este script."
    exit
fi

if [[ ! -e "$PRINCE" ]]; then
    echo "Instale a ferramenta PRINCE (www.princexml.com)."
    echo "Verifique se o programa foi instalado em: $PRINCE"
    echo "Caso contrário, altere o caminho do executável no script."
    exit
fi

if [[ -z "$USUARIO" && -z "$SENHA" ]]; then
    echo "Edite o script e defina as senhas de acesso ao TetraAnalytix."
    exit
fi

MODELO_FINAIS=modelo_traducao/relatorio_anos_finais/
MODELO_INICIAIS=modelo_traducao/relatorio_anos_iniciais/
MODELO_MEDIO=modelo_traducao/relatorio_ensino_medio/

if [[ ! -z "$USUARIO" && ! -z "$SENHA" ]]; then
    echo Coletando dados. Aguarde...
    $CASPERJS_PATH coleta.js --username=$USUARIO --password=$SENHA --grade=1 --report=2 > casperjs_coleta_grade1_report2.log 2>&1
    $CASPERJS_PATH coleta.js --username=$USUARIO --password=$SENHA --grade=1 --report=3 > casperjs_coleta_grade1_report3.log 2>&1
    $CASPERJS_PATH coleta.js --username=$USUARIO --password=$SENHA --grade=2 --report=2 > casperjs_coleta_grade2_report2.log 2>&1
    $CASPERJS_PATH coleta.js --username=$USUARIO --password=$SENHA --grade=2 --report=3 > casperjs_coleta_grade2_report3.log 2>&1
fi

# Copy translation models
for i in casper/*/Grades_2_-_5/* ; do
    if [[ -e $i ]]; then
        cp -R $MODELO_INICIAIS $i;
    fi
done
for i in casper/*/Grades_6_-_9/* ; do
    if [[ -e $i ]]; then
        cp -R $MODELO_FINAIS $i;
    fi
done
for i in casper/*/Grades_10_-_12/* ; do
    if [[ -e $i ]]; then
        cp -R $MODELO_MEDIO $i;
    fi
done

# Generate HTML from PHP
ROOT=$(pwd)
echo Gerando relatórios:
for REPORT_PATH in casper/*/Grades_2_-_5/* casper/*/Grades_6_-_9/* casper/*/Grades_10_-_12/* ; do
    if [[ -d "$ROOT/$REPORT_PATH" ]]; then
        cd "$ROOT/$REPORT_PATH"

        for file in *.php ; do
            file_basename=$(basename "$file")
            filename="${file_basename%.*}"
            php "$filename.php" > "$filename.html"
        done

        REPORT_DATA=${REPORT_PATH/Grades_2_-_5/Anos_Iniciais}
        REPORT_DATA=${REPORT_DATA/Grades_6_-_9/Anos_Finais}
        REPORT_DATA=${REPORT_DATA/Grades_10_-_12/Médio}

        IFS='/' read -r -a DATA <<< "$REPORT_DATA"

        $PRINCE -v -i html --media=print --page-size=US-Letter --javascript \
            --pdf-title="Relatório Projeto CEAP - ${DATA[1]} - ${DATA[2]} - ${DATA[3]}" \
            --pdf-subject="Relatório Projeto CEAP - ${DATA[1]} - ${DATA[2]} - ${DATA[3]}" \
            --pdf-author="Projeto CEAP" \
            "$ROOT/$REPORT_PATH/"*.html \
            -o $ROOT/Relatório_Questionário_CEAP-"${DATA[1]}"-"${DATA[3]}"-"${DATA[2]}".pdf \
            --log=Relatório_Questionário_CEAP-${DATA[1]}-${DATA[3]}-${DATA[2]}.log
        echo Relatório gerado: Relatório_Questionário_CEAP-"${DATA[1]}"-"${DATA[3]}"-"${DATA[2]}".pdf
    fi
done

echo Relatórios gerados!