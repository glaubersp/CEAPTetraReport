'use strict'

var moment = require('moment')
var casper = require('casper').create({
  verbose: true,
  logLevel: 'debug',
  waitTimeout: 20000,
  // Viewport must hold the entire page to be able to capture canvas
  viewportSize: {
    width: 1200,
    height: 1500
  }
})
var x = require('casper').selectXPath
var fs = require('fs')
var path = {
  school: '',
  grade: '',
  time: '',
  dir: function () {
    return 'casper/'.concat(this.school).concat('/').concat(this.grade).concat('/').concat(this.time).concat('/')
  },
  html: function () {
    return this.dir().concat('/').concat('html').concat('/')
  },
  images: function () {
    return this.dir().concat('/').concat('images').concat('/')
  }
}
// Options that should be passed in CLI for CasperJS
var options = {
  // Choose 1 for Grades 6-9; 2 for Grades 2-5; 3 for Grades 10-12
  grade: 1,
  // Choose the report to be opened. Starts with 2 for the latest/first report in the list
  report: 2,
  username: '',
  password: '',
  school_name: '',
  report_date: '',
  grade_name: ''
}

if (casper.cli.has('grade')) {
  options.grade = casper.cli.get('grade')
  casper.log('Option grade: ' + casper.cli.get('grade'), 'debug')
}

if (casper.cli.has('report')) {
  options.report = casper.cli.get('report')
  casper.log('Option report: ' + casper.cli.get('report'), 'debug')
}

if (casper.cli.has('username')) {
  options.username = casper.cli.get('username')
  casper.log('Option username: ' + casper.cli.get('username'), 'debug')
}

if (casper.cli.has('password')) {
  options.password = casper.cli.get('password')
  casper.log('Option password: ' + casper.cli.get('password'), 'debug')
}

casper.options.pageSettings = {
  userName: options.username,
  password: options.password
}

if (casper.cli.has('school_name')) {
  options.school_name = casper.cli.get('school_name')
  casper.log('Option school_name: ' + casper.cli.get('school_name'), 'debug')
}

if (casper.cli.has('grade_name')) {
  options.grade_name = casper.cli.get('grade_name')
  casper.log('Option grade_name: ' + casper.cli.get('grade_name'), 'debug')
}

if (casper.cli.has('report_date')) {
  options.report_date = casper.cli.get('report_date')
  casper.log('Option report_date: ' + casper.cli.get('report_date'), 'debug')
}

casper.on('page.error', function (msg, trace) {
  this.echo('Error: ' + msg, 'ERROR')
  for (var i = 0; i < trace.length; i++) {
    var step = trace[i]
    this.echo('   ' + step.file + ' (line ' + step.line + ')', 'ERROR')
  }
})

casper.start('https://www.tetraanalytix.com/portal/#/login')

// Login
casper.waitForSelector('form[name=loginForm] input[name="username"]', function () {
  this.fillSelectors('form[name=loginForm]', {
    'input[name="username"]': options.username,
    'input[name="password"]': options.password
  }, false)
  this.click(x('/html/body/div[1]/div/div/div/div/div/div/form/div[3]/button'))
})

// Open Grades report.
casper.waitForSelector(x('/html/body/div[1]/div/div[1]/div/div[1]/md-content/md-list/md-list-item[' + options.grade + ']'), function () {
  this.click(x('/html/body/div[1]/div/div[1]/div/div[1]/md-content/md-list/md-list-item[' + options.grade + ']/button'))
  this.wait(10000)
})

// Open the chosen report
casper.waitForSelector(x('//*[@id="tab-content-1"]/div/div/ul[3]/li[' + options.report + ']/div[4]/p[2]'), function () {
  var dateDiv = this.getHTML(x('//*[@id="tab-content-1"]/div/div/ul[3]/li[' + options.report + ']/div[4]/p[2]'))
  var nameDiv = this.getHTML(x('/html/body/div[1]/div/div[1]/div/div[3]/div[1]/div[2]/h3'))
  moment.locale('pt-br')
  var date = moment(new Date(dateDiv), 'MMMM DDD, YYYY')
  path.time = date.format('YYYY_MM_DD')
  path.school = nameDiv.replace(/ /g, '_').split(/_-_/g)[0].toLowerCase()
  path.grade = this.getHTML(x('/html/body/div[1]/div/div[1]/div/div[3]/div[1]/div[2]/p[2]')).replace(/ /g, '_')
  options.report_date = date.format('LL')
  options.school_name = nameDiv.split(/ - /g)[0]
  options.grade_name = nameDiv.split(/ - /g)[1]

  var f = fs.open(path.html().concat('school_name.html'), 'w')
  f.write(options.school_name)
  f.close()
  f = fs.open(path.html().concat('grade_name.html'), 'w')
  f.write(options.grade_name)
  f.close()
  f = fs.open(path.html().concat('report_date.html'), 'w')
  f.write(options.report_date)
  f.close()
})

casper.waitForSelector(x('//*[@id="tab-content-1"]/div/div/ul[3]/li[' + options.report + ']/div[5]'), function () {
  this.open('https://www.tetraanalytix.com'.concat(this.getElementsAttribute(x('//*[@id="tab-content-1"]/div/div/ul[3]/li[' + options.report + ']/div[5]/a[2]'), 'href')))
})

// Report window
// Print summary data
casper.waitForSelector(x('//*[@id="chart-table"]'), function () {
  // One must wait to JS script to run
  this.wait(20000, function () {
    var html = this.getHTML(x('//*[@id="num-students"]'), true)
    var f = fs.open(path.html().concat('num-students.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="num-teachers"]'), true)
    f = fs.open(path.html().concat('num-teachers.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="num-parents"]'), true)
    f = fs.open(path.html().concat('num-parents.html'), 'w')
    f.write(html)
    f.close()

    html = this.getHTML(x('//*[@id="S_CE_AC"]'), true)
    f = fs.open(path.html().concat('num-S_CE_AC.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="T_CE_IT"]'), true)
    f = fs.open(path.html().concat('num-T_CE_IT.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="P_CE_IV"]'), true)
    f = fs.open(path.html().concat('num-P_CE_IV.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="S_SB_AC"]'), true)
    f = fs.open(path.html().concat('num-S_SB_AC.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="T_SB_IT"]'), true)
    f = fs.open(path.html().concat('num-T_SB_IT.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="P_SB_IV"]'), true)
    f = fs.open(path.html().concat('num-P_SB_IV.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="S_RR_AC"]'), true)
    f = fs.open(path.html().concat('num-S_RR_AC.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="T_RR_IT"]'), true)
    f = fs.open(path.html().concat('num-T_RR_IT.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="P_RR_IV"]'), true)
    f = fs.open(path.html().concat('num-P_RR_IV.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="S_PR_AC"]'), true)
    f = fs.open(path.html().concat('num-S_PR_AC.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="T_PR_IT"]'), true)
    f = fs.open(path.html().concat('num-T_PR_IT.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="P_PR_IV"]'), true)
    f = fs.open(path.html().concat('num-P_PR_IV.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="S_CE_IP"]'), true)
    f = fs.open(path.html().concat('num-S_CE_IP.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="T_CE_CL"]'), true)
    f = fs.open(path.html().concat('num-T_CE_CL.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="P_CE_CM"]'), true)
    f = fs.open(path.html().concat('num-P_CE_CM.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="S_SB_IP"]'), true)
    f = fs.open(path.html().concat('num-S_SB_IP.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="T_SB_CL"]'), true)
    f = fs.open(path.html().concat('num-T_SB_CL.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="P_SB_CM"]'), true)
    f = fs.open(path.html().concat('num-P_SB_CM.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="S_RR_IP"]'), true)
    f = fs.open(path.html().concat('num-S_RR_IP.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="T_RR_CL"]'), true)
    f = fs.open(path.html().concat('num-T_RR_CL.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="P_RR_CM"]'), true)
    f = fs.open(path.html().concat('num-P_RR_CM.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="S_PR_IP"]'), true)
    f = fs.open(path.html().concat('num-S_PR_IP.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="T_PR_CL"]'), true)
    f = fs.open(path.html().concat('num-T_PR_CL.html'), 'w')
    f.write(html)
    f.close()
    html = this.getHTML(x('//*[@id="P_PR_CM"]'), true)
    f = fs.open(path.html().concat('num-P_PR_CM.html'), 'w')
    f.write(html)
    f.close()
  })
})

// Print Conditions for learning
casper.waitForSelector(x('//*[@id="report-side-nav"]/ul/a[3]'), function () {
  this.click(x('//*[@id="report-side-nav"]/ul/a[3]'))
  this.waitForSelector(x('//*[@id="S_AC_chart"]'), function () {
    // One must wait to JS script to run
    this.wait(10000, function () {
      var html = this.getHTML(x('//*[@id="S_AC_chart"]'), true)
      var f = fs.open(path.html().concat('S_AC_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('S_AC_triangle.png'), x('//*[@id="S_AC_triangle"]'))
      html = this.getHTML(x('//*[@id="S_IP_chart"]'), true)
      f = fs.open(path.html().concat('S_IP_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('S_IP_triangle.png'), x('//*[@id="S_IP_triangle"]'))
    })
  })
})

// Print Conditions for teaching
casper.waitForSelector(x('//*[@id="report-side-nav"]/ul/a[4]'), function () {
  this.click(x('//*[@id="report-side-nav"]/ul/a[4]'))
  this.waitForSelector(x('//*[@id="T_IT_chart"]'), function () {
    // One must wait to JS script to run
    this.wait(10000, function () {
      var html = this.getHTML(x('//*[@id="T_IT_chart"]'), true)
      var f = fs.open(path.html().concat('T_IT_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('T_IT_triangle.png'), x('//*[@id="T_IT_triangle"]'))
      html = this.getHTML(x('//*[@id="T_CL_chart"]'), true)
      f = fs.open(path.html().concat('T_CL_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('T_CL_triangle.png'), x('//*[@id="T_CL_triangle"]'))
    })
  })
})

// Print Conditions for community
casper.waitForSelector(x('//*[@id="report-side-nav"]/ul/a[5]'), function () {
  this.click(x('//*[@id="report-side-nav"]/ul/a[5]'))
  this.waitForSelector(x('//*[@id="P_IV_chart"]'), function () {
    // One must wait to JS script to run
    this.wait(10000, function () {
      var html = this.getHTML(x('//*[@id="P_IV_chart"]'), true)
      var f = fs.open(path.html().concat('P_IV_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('P_IV_triangle.png'), x('//*[@id="P_IV_triangle"]'))
      html = this.getHTML(x('//*[@id="P_CM_chart"]'), true)
      f = fs.open(path.html().concat('P_CM_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('P_CM_triangle.png'), x('//*[@id="P_CM_triangle"]'))
    })
  })
})

// Print Academic Skills
casper.waitForSelector(x('//*[@id="report-side-nav"]/ul/a[6]'), function () {
  this.click(x('//*[@id="report-side-nav"]/ul/a[6]'))
  this.waitForSelector(x('//*[@id="S_CE_AC_chart"]'), function () {
    // One must wait to JS script to run
    this.wait(10000, function () {
      var html = this.getHTML(x('//*[@id="S_CE_AC_chart"]'), true)
      var f = fs.open(path.html().concat('S_CE_AC_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('S_CE_AC_slide.png'), x('//*[@id="S_CE_AC_slide"]'))
      html = this.getHTML(x('//*[@id="S_CE_AC_slide"]'), true)

      html = this.getHTML(x('//*[@id="S_SB_AC_chart"]'), true)
      f = fs.open(path.html().concat('S_SB_AC_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('S_SB_AC_slide.png'), x('//*[@id="S_SB_AC_slide"]'))
      html = this.getHTML(x('//*[@id="S_SB_AC_slide"]'), true)

      html = this.getHTML(x('//*[@id="S_RR_AC_chart"]'), true)
      f = fs.open(path.html().concat('S_RR_AC_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('S_RR_AC_slide.png'), x('//*[@id="S_RR_AC_slide"]'))
      html = this.getHTML(x('//*[@id="S_RR_AC_slide"]'), true)

      html = this.getHTML(x('//*[@id="S_PR_AC_chart"]'), true)
      f = fs.open(path.html().concat('S_PR_AC_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('S_PR_AC_slide.png'), x('//*[@id="S_PR_AC_slide"]'))
      html = this.getHTML(x('//*[@id="S_PR_AC_slide"]'), true)
    })
  })
})

// Print Interpersonal Skills
casper.waitForSelector(x('//*[@id="report-side-nav"]/ul/a[7]'), function () {
  this.click(x('//*[@id="report-side-nav"]/ul/a[7]'))
  this.waitForSelector(x('//*[@id="S_CE_IP_chart"]'), function () {
    // One must wait to JS script to run
    this.wait(10000, function () {
      var html = this.getHTML(x('//*[@id="S_CE_IP_chart"]'), true)
      var f = fs.open(path.html().concat('S_CE_IP_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('S_CE_IP_slide.png'), x('//*[@id="S_CE_IP_slide"]'))
      html = this.getHTML(x('//*[@id="S_CE_IP_slide"]'), true)

      html = this.getHTML(x('//*[@id="S_SB_IP_chart"]'), true)
      f = fs.open(path.html().concat('S_SB_IP_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('S_SB_IP_slide.png'), x('//*[@id="S_SB_IP_slide"]'))
      html = this.getHTML(x('//*[@id="S_SB_IP_slide"]'), true)

      html = this.getHTML(x('//*[@id="S_RR_IP_chart"]'), true)
      f = fs.open(path.html().concat('S_RR_IP_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('S_RR_IP_slide.png'), x('//*[@id="S_RR_IP_slide"]'))
      html = this.getHTML(x('//*[@id="S_RR_IP_slide"]'), true)

      html = this.getHTML(x('//*[@id="S_PR_IP_chart"]'), true)
      f = fs.open(path.html().concat('S_PR_IP_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('S_PR_IP_slide.png'), x('//*[@id="S_PR_IP_slide"]'))
      html = this.getHTML(x('//*[@id="S_PR_IP_slide"]'), true)
    })
  })
})

// Print Instructional Support
casper.waitForSelector(x('//*[@id="report-side-nav"]/ul/a[8]'), function () {
  this.click(x('//*[@id="report-side-nav"]/ul/a[8]'))
  this.waitForSelector(x('//*[@id="T_CE_IT_chart"]'), function () {
    // One must wait to JS script to run
    this.wait(10000, function () {
      var html = this.getHTML(x('//*[@id="T_CE_IT_chart"]'), true)
      var f = fs.open(path.html().concat('T_CE_IT_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('T_CE_IT_slide.png'), x('//*[@id="T_CE_IT_slide"]'))
      html = this.getHTML(x('//*[@id="T_CE_IT_slide"]'), true)

      html = this.getHTML(x('//*[@id="T_SB_IT_chart"]'), true)
      f = fs.open(path.html().concat('T_SB_IT_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('T_SB_IT_slide.png'), x('//*[@id="T_SB_IT_slide"]'))
      html = this.getHTML(x('//*[@id="T_SB_IT_slide"]'), true)

      html = this.getHTML(x('//*[@id="T_RR_IT_chart"]'), true)
      f = fs.open(path.html().concat('T_RR_IT_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('T_RR_IT_slide.png'), x('//*[@id="T_RR_IT_slide"]'))
      html = this.getHTML(x('//*[@id="T_RR_IT_slide"]'), true)

      html = this.getHTML(x('//*[@id="T_PR_IT_chart"]'), true)
      f = fs.open(path.html().concat('T_PR_IT_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('T_PR_IT_slide.png'), x('//*[@id="T_PR_IT_slide"]'))
      html = this.getHTML(x('//*[@id="T_PR_IT_slide"]'), true)
    })
  })
})

// Print Collaboration
casper.waitForSelector(x('//*[@id="report-side-nav"]/ul/a[9]'), function () {
  this.click(x('//*[@id="report-side-nav"]/ul/a[9]'))
  this.waitForSelector(x('//*[@id="T_CE_CL_chart"]'), function () {
    // One must wait to JS script to run
    this.wait(10000, function () {
      var html = this.getHTML(x('//*[@id="T_CE_CL_chart"]'), true)
      var f = fs.open(path.html().concat('T_CE_CL_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('T_CE_CL_slide.png'), x('//*[@id="T_CE_CL_slide"]'))
      html = this.getHTML(x('//*[@id="T_CE_CL_slide"]'), true)

      html = this.getHTML(x('//*[@id="T_SB_CL_chart"]'), true)
      f = fs.open(path.html().concat('T_SB_CL_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('T_SB_CL_slide.png'), x('//*[@id="T_SB_CL_slide"]'))
      html = this.getHTML(x('//*[@id="T_SB_CL_slide"]'), true)

      html = this.getHTML(x('//*[@id="T_RR_CL_chart"]'), true)
      f = fs.open(path.html().concat('T_RR_CL_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('T_RR_CL_slide.png'), x('//*[@id="T_RR_CL_slide"]'))
      html = this.getHTML(x('//*[@id="T_RR_CL_slide"]'), true)

      html = this.getHTML(x('//*[@id="T_PR_CL_chart"]'), true)
      f = fs.open(path.html().concat('T_PR_CL_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('T_PR_CL_slide.png'), x('//*[@id="T_PR_CL_slide"]'))
      html = this.getHTML(x('//*[@id="T_PR_CL_slide"]'), true)
    })
  })
})

// Print Parent Involvement
casper.waitForSelector(x('//*[@id="report-side-nav"]/ul/a[10]'), function () {
  this.click(x('//*[@id="report-side-nav"]/ul/a[10]'))
  this.waitForSelector(x('//*[@id="P_CE_IV_chart"]'), function () {
    // One must wait to JS script to run
    this.wait(10000, function () {
      var html = this.getHTML(x('//*[@id="P_CE_IV_chart"]'), true)
      var f = fs.open(path.html().concat('P_CE_IV_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('P_CE_IV_slide.png'), x('//*[@id="P_CE_IV_slide"]'))
      html = this.getHTML(x('//*[@id="P_CE_IV_slide"]'), true)

      html = this.getHTML(x('//*[@id="P_SB_IV_chart"]'), true)
      f = fs.open(path.html().concat('P_SB_IV_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('P_SB_IV_slide.png'), x('//*[@id="P_SB_IV_slide"]'))
      html = this.getHTML(x('//*[@id="P_SB_IV_slide"]'), true)

      html = this.getHTML(x('//*[@id="P_RR_IV_chart"]'), true)
      f = fs.open(path.html().concat('P_RR_IV_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('P_RR_IV_slide.png'), x('//*[@id="P_RR_IV_slide"]'))
      html = this.getHTML(x('//*[@id="P_RR_IV_slide"]'), true)

      html = this.getHTML(x('//*[@id="P_PR_IV_chart"]'), true)
      f = fs.open(path.html().concat('P_PR_IV_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('P_PR_IV_slide.png'), x('//*[@id="P_PR_IV_slide"]'))
      html = this.getHTML(x('//*[@id="P_PR_IV_slide"]'), true)
    })
  })
})

// Print Community Support
casper.waitForSelector(x('//*[@id="report-side-nav"]/ul/a[11]'), function () {
  this.click(x('//*[@id="report-side-nav"]/ul/a[11]'))
  this.waitForSelector(x('//*[@id="P_CE_CM_chart"]'), function () {
    // One must wait to JS script to run
    this.wait(10000, function () {
      var html = this.getHTML(x('//*[@id="P_CE_CM_chart"]'), true)
      var f = fs.open(path.html().concat('P_CE_CM_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('P_CE_CM_slide.png'), x('//*[@id="P_CE_CM_slide"]'))
      html = this.getHTML(x('//*[@id="P_CE_CM_slide"]'), true)

      html = this.getHTML(x('//*[@id="P_SB_CM_chart"]'), true)
      f = fs.open(path.html().concat('P_SB_CM_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('P_SB_CM_slide.png'), x('//*[@id="P_SB_CM_slide"]'))
      html = this.getHTML(x('//*[@id="P_SB_CM_slide"]'), true)

      html = this.getHTML(x('//*[@id="P_RR_CM_chart"]'), true)
      f = fs.open(path.html().concat('P_RR_CM_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('P_RR_CM_slide.png'), x('//*[@id="P_RR_CM_slide"]'))
      html = this.getHTML(x('//*[@id="P_RR_CM_slide"]'), true)

      html = this.getHTML(x('//*[@id="P_PR_CM_chart"]'), true)
      f = fs.open(path.html().concat('P_PR_CM_chart.html'), 'w')
      f.write(html)
      f.close()
      this.captureSelector(path.images().concat('P_PR_CM_slide.png'), x('//*[@id="P_PR_CM_slide"]'))
      html = this.getHTML(x('//*[@id="P_PR_CM_slide"]'), true)
    })
  })
})

// Print Safety, Instructional Quality, & Leadership
casper.waitForSelector(x('//*[@id="report-side-nav"]/ul/a[12]'), function () {
  this.click(x('//*[@id="report-side-nav"]/ul/a[12]'))
  this.waitForSelector(x('//*[@id="S_1_gold_slide"]'), function () {
    // One must wait to JS script to run
    this.wait(10000, function () {
      this.captureSelector(path.images().concat('S_1_gold_slide.png'), x('//*[@id="S_1_gold_slide"]'))
      this.captureSelector(path.images().concat('T_1_gold_slide.png'), x('//*[@id="T_1_gold_slide"]'))
      this.captureSelector(path.images().concat('P_1_gold_slide.png'), x('//*[@id="P_1_gold_slide"]'))

      this.captureSelector(path.images().concat('S_2_gold_slide.png'), x('//*[@id="S_2_gold_slide"]'))
      this.captureSelector(path.images().concat('T_2_gold_slide.png'), x('//*[@id="T_2_gold_slide"]'))
      this.captureSelector(path.images().concat('P_2_gold_slide.png'), x('//*[@id="P_2_gold_slide"]'))

      this.captureSelector(path.images().concat('S_3_gold_slide.png'), x('//*[@id="S_3_gold_slide"]'))
      this.captureSelector(path.images().concat('T_3_gold_slide.png'), x('//*[@id="T_3_gold_slide"]'))
      this.captureSelector(path.images().concat('P_3_gold_slide.png'), x('//*[@id="P_3_gold_slide"]'))

      this.captureSelector(path.images().concat('S_4_gold_slide.png'), x('//*[@id="S_4_gold_slide"]'))
      this.captureSelector(path.images().concat('T_4_gold_slide.png'), x('//*[@id="T_4_gold_slide"]'))
      this.captureSelector(path.images().concat('P_4_gold_slide.png'), x('//*[@id="P_4_gold_slide"]'))
    })
  })
})

// Print Demographics
casper.waitForSelector(x('//*[@id="report-side-nav"]/ul/a[13]'), function () {
  this.click(x('//*[@id="report-side-nav"]/ul/a[13]'))
  this.waitForSelector(x('//*[@id="demo-grade"]'), function () {
    this.wait(10000, function () {
      var html = this.getHTML(x('//*[@id="demo-grade"]'), true)
      var f = fs.open(path.html().concat('demo-grade.html'), 'w')
      f.write(html)
      f.close()
      html = this.getHTML(x('//*[@id="demo-ethnicity"]'), true)
      f = fs.open(path.html().concat('demo-ethnicity.html'), 'w')
      f.write(html)
      f.close()
    })
  })
})

// Print Text Responses
casper.waitForSelector(x('//*[@id="report-side-nav"]/ul/a[14]'), function () {
  this.click(x('//*[@id="report-side-nav"]/ul/a[14]'))
  this.waitForSelector(x('//*[@id="text-student"]'), function () {
    // One must wait to JS script to run
    this.wait(10000, function () {
      this.captureSelector(path.images().concat('text-student.png'), x('//*[@id="text-student"]'))
      var html = this.getHTML(x('//*[@id="text-student"]'), true)
      var f = fs.open(path.html().concat('text-student.html'), 'w')
      f.write(html)
      f.close()

      this.captureSelector(path.images().concat('text-teacher.png'), x('//*[@id="text-teacher"]'))
      html = this.getHTML(x('//*[@id="text-teacher"]'), true)
      f = fs.open(path.html().concat('text-teacher.html'), 'w')
      f.write(html)
      f.close()

      this.captureSelector(path.images().concat('text-parent.png'), x('//*[@id="text-parent"]'))
      html = this.getHTML(x('//*[@id="text-parent"]'), true)
      f = fs.open(path.html().concat('text-parent.html'), 'w')
      f.write(html)
      f.close()
    })
  })
})

casper.run(function () {
  this.log('Terminou.', 'INFO')
  this.exit()
})